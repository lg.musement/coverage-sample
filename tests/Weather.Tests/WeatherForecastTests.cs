using Xunit;

namespace Weather.Tests
{
    public class WeatherForecastTests
    {
        [Fact]
        public void WeatherForecast_CorrectlyComputesFahrenheitTemperature()
        {
            var forecast = new WeatherForecast()
            {
                TemperatureC = 20
            };
            
            Assert.Equal(67, forecast.TemperatureF);
        }
    }
}
